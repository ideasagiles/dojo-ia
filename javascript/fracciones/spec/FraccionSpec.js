describe("Fraccion", function() {
  var fraccion;

  describe("constructor", function() {
    it("Crea la fracción irreducible", function() {
      fraccion = new Fraccion(2,3);

      expect(fraccion.numerador).toBe(2);
      expect(fraccion.denominador).toBe(3);
    });
  });
});
