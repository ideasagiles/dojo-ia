package com.ideasagiles;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * El test para la clase Fraccion.
 */
public class FraccionTest {

    @Test
    public void constructor_fraccionIrreducible_creaLaFraccion() {
        int numerador = 2;
        int denominador = 3;

        Fraccion fraccion = new Fraccion(numerador, denominador);

        assertEquals(numerador, fraccion.getNumerador());
        assertEquals(denominador, fraccion.getDenominador());
    }


}
