package com.ideasagiles;

/**
 * Representa una fraccion matematica, la division entre el numerador y el
 * denominador, de forma exacta.
 * En la fraccion "3/4", el numero "3" es el numerador, y el numero "4" es el
 * denominador.
 *
 */
public class Fraccion {

    private int numerador;
    private int denominador;

    public Fraccion(int numerador, int denominador) {
        this.numerador = numerador;
        this.denominador = denominador;
    }

    public int getDenominador() {
        return denominador;
    }

    public int getNumerador() {
        return numerador;
    }

}
